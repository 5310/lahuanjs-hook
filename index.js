(function() {
    
    

    /**
     * ***This is merely a dummy repository containing a useless module for reference.***
     * 
     * Bare-bones event-emitter class and singleton instance.
     * 
     * [![Build](http://img.shields.io/travis/lahuan/lahuanjs-hook.svg?style=flat)](https://travis-ci.org/lahuan/lahuanjs-hook)
     * [![Dependencies](http://img.shields.io/david/lahuan/lahuanjs-hook.svg?style=flat)](https://david-dm.org/lahuan/lahuanjs-hook)
     * [![License](http://img.shields.io/badge/license-mit-green.svg?style=flat)](https://github.com/lahuan/lahuanjs-hook/blob/master/LICENSE)
     * [![Release](http://img.shields.io/badge/release-v0.1.0-orange.svg?style=flat)](https://github.com/lahuan/lahuanjs-hook/releases)
     * 
     * @module lahuanjs-hook
     * 
     * @alias Hook
     * 
     * @example
     * 
     *  Use of module as a central singleton for triggering and binding hooks:
     * 
     *  ```js
     *  Hook
     *      .bind( 'TestHook', function( name ) { console.log("Hullo "+name+"!"); } )
     *      .trigger( 'TestHook', "World" );
     *  // "Hullo World!"
     *  ```
     *  
     *  Use of instances as members of other objects to provide binding and triggering of hooks:
     * 
     *  ```js
     *  var x = {};
     *  x.counter = 0;
     *  x.hook = new Hook( x );
     * 
     *  x.hook.bind( 'CountUp', function( step ) { 
     *      var step = typeof step === 'number' ? step : 1;
     *      this.counter += step;
     *      console.log("Current count: "+this.counter); 
     *  } )
     * 
     *  x.hook
     *      .trigger( 'CountUp' )
     *      .trigger( 'CountUp', 10 );
     *  // "Current count: 1
     *  // "Current count: 11
     *  ```
     * 
     *  Usage in Node:
     *  
     *  ```js
     *  var Hook = require('lahuanjs-hook');
     *  //...
     *  ```
     * 
     *  Usage in the browser via UMD build:
     *  
     *  ```html
     *  <script src="<path>/lahuanjs-hook.js"></script>
     *  <script>
     *      var Hook = lahuanjs.Hook;
     *      //...
     *  </script>
     *  ```
     */
    
    require('lahuanjs-com'); //NOTE: To test Browserify externalization.
    
    /**
     * Constructs a hook object that other methods can bind callbacks to under specific hooknames and trigger them.
     * 
     * @param {object} [context=this] An optional context that overrides the context under which all the callbacks are triggered.
     * 
     * @constructor
     * @alias module:lahuanjs-hook
     */
    var Hook = function( context ) {
        /**
         * The context for this hook object under which all the callbacks are run.
         * @private
         */
        this._context = this;
        if ( typeof context === 'object' || typeof context === 'function' ) {
            this._context = context;
        }
        /**
         * The listindex of all registered hooks 
         * by hookname and the list of that hookname's registered callbacks.
         * @private
         */
        this._hooks = Object.create(null);
    };
    
    /**
     * Binds a callback to a hookname.
     * 
     * @param {string} hookname An arbitrary string signifying an unique hook.
     * @param {function} callback An arbitrary function that will be bound to the hookname and triggered when it is triggered.
     * 
     * @returns Returns itself for chainability.
     */
    Hook.prototype.bind = function( hookname, callback ) {
        var hooks = this._hooks;
        if ( !( hookname in hooks ) ) {
            hooks[hookname] = [];
        }
        hooks[hookname].push(callback);
        return this;
    };
    
    /**
     * Triggers a hookname, invoking all its callbacks.
     * 
     * @param {string} hookname An arbitrary string signifying an unique hook.
     * @param {object} [payload] An arbitrary reference which will be passed down to all callbacks triggered under this hookname.
     * 
     * @returns Returns itself for chainability.
     */
    Hook.prototype.trigger = function( hookname, payload ) {
        var callbacks = this._hooks[hookname];
        for ( var i in callbacks ) {
            callbacks[i].call(this._context, payload);
        }
        return this;
    };
    
    /**
     * Unbinds a callback from a hookname.
     * 
     * @param {string} hookname An arbitrary string signifying an unique hook.
     * @param {function} callback An arbitrary function which will be removed from the hook's registry.
     * 
     * @returns Returns itself for chainability.
     */
    Hook.prototype.unbind = function( hookname, callback ) {
	    var callbacks = this._hooks[hookname];
        for ( var i in callbacks ) {
            if ( callback === callbacks[i] ) {
                callbacks.splice(i, 1);
            }
        }
        return this;
    };
    
    
    
    /*
     * The singleton instance of the hook object 
     * that is accessible statically from this module.
     */
    var singletonInstance = new Hook( );
    
    
    
    /*
     * The export object.
     */
    module.exports = Hook;
    
    /**
     * Binds a callback to the singleton instance of this module. See the instance method {@link module:lahuanjs-hook#bind}.
     */
    module.exports.bind = singletonInstance.bind.bind(singletonInstance);
    
    /**
     * Triggers a hookname of the singleton instance of this module. See the instance method {@link module:lahuanjs-hook#trigger}.
     */
    module.exports.trigger = singletonInstance.trigger.bind(singletonInstance);
    
    /**
     * Unbinds a callback from a hookname of the singleton instance of this module. See the instance method {@link module:lahuanjs-hook#unbind}.
     */
    module.exports.unbind = singletonInstance.unbind.bind(singletonInstance);
    
    

}).call(this);