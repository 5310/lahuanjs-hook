<a name="module_lahuanjs-hook"></a>
#lahuanjs-hook
***This is merely a dummy repository containing a useless module for reference.***

Bare-bones event-emitter class and singleton instance.

[![Build](http://img.shields.io/travis/lahuan/lahuanjs-hook.svg?style=flat)](https://travis-ci.org/lahuan/lahuanjs-hook)
[![Dependencies](http://img.shields.io/david/lahuan/lahuanjs-hook.svg?style=flat)](https://david-dm.org/lahuan/lahuanjs-hook)
[![License](http://img.shields.io/badge/license-mit-green.svg?style=flat)](https://github.com/lahuan/lahuanjs-hook/blob/master/LICENSE)
[![Release](http://img.shields.io/badge/release-v0.1.0-orange.svg?style=flat)](https://github.com/lahuan/lahuanjs-hook/releases)

##Usage

Use of module as a central singleton for triggering and binding hooks:

```js
Hook
    .bind( 'TestHook', function( name ) { console.log("Hullo "+name+"!"); } )
    .trigger( 'TestHook', "World" );
// "Hullo World!"
```

Use of instances as members of other objects to provide binding and triggering of hooks:

```js
var x = {};
x.counter = 0;
x.hook = new Hook( x );

x.hook.bind( 'CountUp', function( step ) { 
    var step = typeof step === 'number' ? step : 1;
    this.counter += step;
    console.log("Current count: "+this.counter); 
} )

x.hook
    .trigger( 'CountUp' )
    .trigger( 'CountUp', 10 );
// "Current count: 1
// "Current count: 11
```

Usage in Node:

```js
var Hook = require('lahuanjs-hook');
//...
```

Usage in the browser via UMD build:

```html
<script src="<path>/lahuanjs-hook.js"></script>
<script>
    var Hook = lahuanjs.Hook;
    //...
</script>
```

##API
<a name="exp_module_lahuanjs-hook"></a>
###class: Hook ⏏
**Members**

* [class: Hook ⏏](#exp_module_lahuanjs-hook)
  * [new Hook([context])](#exp_new_module_lahuanjs-hook)
  * [Hook.bind](#module_lahuanjs-hook.bind)
  * [Hook.trigger](#module_lahuanjs-hook.trigger)
  * [Hook.unbind](#module_lahuanjs-hook.unbind)
  * [hook.bind(hookname, callback)](#module_lahuanjs-hook#bind)
  * [hook.trigger(hookname, [payload])](#module_lahuanjs-hook#trigger)
  * [hook.unbind(hookname, callback)](#module_lahuanjs-hook#unbind)

<a name="exp_new_module_lahuanjs-hook"></a>
####new Hook([context])
Constructs a hook object that other methods can bind callbacks to under specific hooknames and trigger them.

#####Params

- _\[context=this\]_ `object` - An optional context that overrides the context under which all the callbacks are triggered.  


<a name="module_lahuanjs-hook.bind"></a>
####Hook.bind
Binds a callback to the singleton instance of this module. See the instance method [bind](#module_lahuanjs-hook#bind).

<a name="module_lahuanjs-hook.trigger"></a>
####Hook.trigger
Triggers a hookname of the singleton instance of this module. See the instance method [trigger](#module_lahuanjs-hook#trigger).

<a name="module_lahuanjs-hook.unbind"></a>
####Hook.unbind
Unbinds a callback from a hookname of the singleton instance of this module. See the instance method [unbind](#module_lahuanjs-hook#unbind).

<a name="module_lahuanjs-hook#bind"></a>
####hook.bind(hookname, callback)
Binds a callback to a hookname.

#####Params

- _hookname_ `string` - An arbitrary string signifying an unique hook.  
- _callback_ `function` - An arbitrary function that will be bound to the hookname and triggered when it is triggered.  


#####Returns

 - Returns itself for chainability.  

<a name="module_lahuanjs-hook#trigger"></a>
####hook.trigger(hookname, [payload])
Triggers a hookname, invoking all its callbacks.

#####Params

- _hookname_ `string` - An arbitrary string signifying an unique hook.  
- _\[payload\]_ `object` - An arbitrary reference which will be passed down to all callbacks triggered under this hookname.  


#####Returns

 - Returns itself for chainability.  

<a name="module_lahuanjs-hook#unbind"></a>
####hook.unbind(hookname, callback)
Unbinds a callback from a hookname.

#####Params

- _hookname_ `string` - An arbitrary string signifying an unique hook.  
- _callback_ `function` - An arbitrary function which will be removed from the hook's registry.  


#####Returns

 - Returns itself for chainability.  


