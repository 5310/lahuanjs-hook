var test = require( 'tape' ).test;
var Hook = require( '../' );

test("Default Hook object tests.", function (t) {
    
    var p, r;
    
    // Instantiate Hook object.
    
    var hook = new Hook();
    
    t.ok( hook instanceof Hook, "Hook object instantiated." );
    
    // Bind to Hook object.
    
    var hookname = "TestHook";
    var callback = function( payload ) {
        p = payload;
    };
    r = hook.bind( hookname, callback );
    
    t.equal( r, hook, "Binding a hook returns itself for chainability." );
    t.equal( hook._hooks[hookname][0], callback, "Binding a hook registered callback under the bound hookname." );
    
    
    // Trigger Hook object.
    
    r = hook.trigger( hookname );
    
    t.equal( r, hook, "Triggering a hook returns itself for chainability." );
    t.equal( p, undefined, "Hook object triggered without any payload." );
    
    var payload = { data: "This is Data." };
    hook.trigger( hookname, payload );
    
    t.equal( p.data, payload.data, "Hook object triggered with payload." );
    
    // Unbind Hook object.
    
    r = hook.unbind( hookname, callback );
    
    t.equal( r, hook, "Unbinding a hook returns itself for chainability." );
    t.equal( hook._hooks[hookname].length, 0, "Unbinding a hook removed callback from the bound hookname." );
    
    // Tests done.
    t.end()
  
})

test("Context overridden Hook object tests.", function (t) {
    
    
    var p;
    var context = { property: "This is a property." };
    
    // Instantiate Hook object.
    
    var hook = new Hook( context );
    
    t.equal( hook._context, context, "Hook object instantiated with overridden context." );
    
    // Bind to Hook object.
    
    var hookname = "TestHook";
    
    var callback1 = function( payload ) {
        payload.newdata = this.property;
    };
    hook.bind( hookname, callback1 );
    
    var callback2 = function( payload ) {
        p = payload;
    };
    hook.bind( hookname, callback2 );
    
    t.equal( hook._hooks[hookname].length, 2, "Multiple callbacks bound to same hookname." );
    t.ok( 
        hook._hooks[hookname][0] == callback1 && 
        hook._hooks[hookname][1] == callback2, 
        "Bound multiple callbacks registered according to order bound." 
    );
    
    // Trigger Hook object.
    
    var payload = { data: "This is Data." };
    hook.trigger( hookname, payload );
    
    t.equal( p.newdata, "This is a property.", "All callbacks of specified hookname triggered in order and under overridden context." );
    
    // Unbind Hook object.
    
    hook.unbind( hookname, callback1 );
    
    t.equal( hook._hooks[hookname][0], callback2, "Unbinding a hook removed the specified callback from the bound hookname." );
    
    // Tests done.
    t.end()
  
})